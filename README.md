# Translations of l2toxic.com

## About

All the text on the site is divided into sections. One section - one file with the same name.

## Files

The rules is simple - one language - one directory. Directory name is a language shortcut. 
*empty* is a bootstrap directory. You can copy all files from *empty* and write translations
for the messages.

* *account.php* - Common account info
* *balance.php* - Balance page
* *carousel.php* - Screenshots on the main page
* *character.php* - All for the character info and messages of actions in account section
* *email.php* - This text used in e-mail messages
* *index.php* - Common info on index page
* *login.php* - Login page
* *menu.php* - Main menu and account sub menu items
* *news.php* - News on the main page and on news pages
* *registration.php* - Registration and restore password pages
* *statistic.php* - Statistic page

## Structure

All of files for translation - PHP arrays, key => value structure, where key is english text, 
and value - the same text in other language, for example for russian

```
<?php
return [
	'#' => '№',
	'Alliance' => 'Альянс',
	'Castle' => 'Замок',
	'Castles information on' => 'Информация по замкам на сервере',
]
```

--- 
