<?php
return [
	'Change' => '',
	'Change nickname' => '',
	'Character' => '',
	'Character has' => '',
	'Cost' => '',
	'Currency' => '',
	'New&nbsp;name' => '',
	'Server' => '',
	'To change nickname select server, character, enter new name and press button \'Change\''
		=> '',

	'New&nbsp;color' => '',
	'Change nick color' => '',
	'Change title color' => '',
	'To change color select server, character, color and press button \'Change\''
		=> '',

	'Enchant weapon and armor' => '',
	'Enchant shirts, cloaks, bracelets, belts' => '',
	'This item can\'t be enchanted' => '',
	'Item already has maximum value of enchant' => '',
	'Change Attribute' => '',
	'Enchant Attribute' => '',
	'This item can\'t be attributed' => '',
	'Item already has maximum value of attribute enchant'
		=> '',
	'Augmentation' => '',
	'This item can\'t be augmented' => '',
	'Augment' => '',

	'Skills enchant' => '',
	'You can see only enchanted skills here. Please enchant skill to +1 in game before make any changes here'
		=> '',
	'Skills - Change route' => '',
	'Skills enchant after class change' => '',
	'Enchant' => '',

	'Selected character is online! Log out and try again or select another character' => '',

	'New and old name are same' => '',
	'There is an error. Try again later' => '',
	'Character is online! Logout and try again!' => '',
	'Name was changed successfully' => '',
	'Color was changed successfully' => '',
	'Item was enchanted successfully' => '',
	'There is an error. Write to Administration' => '',
	'Сan not modify item' => '',
	'Item was augmented successfully' => '',
	'Attribute was changed successfully' => '',
	'Attribute enchanted successfully' => '',
	'Error' => '',
	'Route of skill was successfully changed' => '',
	'Skill was successfully enchanted' => '',
];
