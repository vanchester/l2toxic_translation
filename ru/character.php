<?php
return [
	'Change' => 'Изменить',
	'Change nickname' => 'Изменить никнейм',
	'Character' => 'Персонаж',
	'Character has' => 'У персонажа',
	'Cost' => 'Цена',
	'Currency' => 'Валюта',
	'New&nbsp;name' => 'Новое&nbsp;имя',
	'Server' => 'Сервер',
	'To change nickname select server, character, enter new name and press button \'Change\''
		=> 'Для изменения ника выберите сервер и персонажа, введите новое имя и нажмите кнопку "Изменить"',

	'New&nbsp;color' => 'Новый&nbsp;цвет',
	'Change nick color' => 'Смена цвета ника',
	'Change title color' => 'Смена цвета титула',
	'To change color select server, character, color and press button \'Change\''
		=> 'Для смены цвета выберите сервер, персонажа и цвет и нажмите кнопку "Изменить"',

	'Enchant weapon and armor' => 'Заточка оружия и брони',
	'Enchant shirts, cloaks, bracelets, belts' => 'Заточка рубашек, плащей, браслетов, поясов',
	'This item can\'t be enchanted' => 'Этот предмет не может быть заточен',
	'Item already has maximum value of enchant' => 'Вещь уже имеет максимальный уровень заточки',
	'Change Attribute' => 'Сменить атрибут',
	'Enchant Attribute' => 'Заточить атрибут',
	'This item can\'t be attributed' => 'В эту вещь нельзя вставить атрибут',
	'Item already has maximum value of attribute enchant'
		=> 'Этот предмет уже имеет максимальное значение заточки атрибута',
	'Augmentation' => 'Аугментация',
	'This item can\'t be augmented' => 'Эта вещь не может быть аугментирована',
	'Augment' => 'Аугментировать',

	'Skills enchant' => 'Заточка скиллов',
	'You can see only enchanted skills here. Please enchant skill to +1 in game before make any changes here'
		=> 'Здесь отображаются только заточенные скиллы. Пожалуйста, заточите скилл на +1 в игре, если вы здесь его не видите',
	'Skills - Change route' => 'Скиллы - смена направления заточки',
	'Skills enchant after class change' => 'Заточка скиллов после смены класса',
	'Enchant' => 'Заточить',

	'Selected character is online! Log out and try again or select another character' => 'Выбранный персонаж в игре! Выйдите из игры и повторите попытку или выберите другого персонажа',

	'New and old name are same' => 'Новое имя совпадает со старым',
	'There is an error. Try again later' => 'Произошла ошибка. Попробуйте позже',
	'Character is online! Logout and try again!' => 'Персонаж в игре. Выйдите из игры и повторите попытку',
	'Name was changed successfully' => 'Имя успешно изменено',
	'Color was changed successfully' => 'Цвет успешно изменен',
	'Item was enchanted successfully' => 'Предмет успешно заточен',
	'There is an error. Write to Administration' => 'Произошла ошибка. Обратитесь к Администратору',
	'Сan not modify item' => 'Не получается изменить предмет',
	'Item was augmented successfully' => 'Аугментация прошла успешно',
	'Attribute was changed successfully' => 'Атрибут успешно изменен',
	'Attribute enchanted successfully' => 'Атрибут успешно заточен',
	'Error' => 'Произошла ошибка',
	'Route of skill was successfully changed' => 'Направление заточки скилла успешно изменено',
	'Skill was successfully enchanted' => 'Скилл успешно заточен'
];
