<?php
return [
	'Change' => 'Изменить',
	'Change password' => 'Изменение пароля',
	'Current password' => 'Текущий пароль',
	'New password' => 'Новый пароль',
	'Repeat new password' => 'Повторите новый пароль',
	'Answer 1' => 'Ответ 1',
	'Answer 2' => 'Ответ 2',
	'Email address is not a valid' => 'Введите корректный e-mail',
	'Forgot password?' => 'Забыли пароль?',
	'ID' => 'ID',
	'Login' => 'Логин',
	'Password' => 'Пароль',
	'Password must be repeated exactly' => 'Введенные пароли не совпадают',
	'Please fill out the following form with your credentials' => 'Пожалуйста, заполните все поля формы',
	'Question 1' => 'Вопрос 1',
	'Question 2' => 'Вопрос 2',
	'Register' => 'Зарегистрироваться',
	'Registration' => 'Регистрация',
	'Server' => 'Сервер',
	'Text from picture' => 'Текст на картинке',
	'User already exists' => 'Пользователь уже существует',
	'Congratulations! Account was registered successfully' => 'Поздравляем! Аккаунт успешно зарегистрирован',
	'Wrong link' => 'Неверная ссылка',
	'Password changed succesfully' => 'Пароль успешно изменен',
	'There is an error. Try again later, please' => 'Произошла ошибка. Повторите попытку позже',
	'Check your e-mail for instructions about how to change the password to your account' => 'Инструкция по смене пароля отправлена на Ваш email',
	'If you can\'t recover password send mail to :mailtoLink' => 'Если у вас не получается восстановить пароль, напишите на :mailtoLink',
	'Password restore' => 'Восстановление пароля',
];
