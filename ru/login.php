<?php
return [
	'Login to site' => 'Авторизация',
	'Account has been blocked' => 'Аккаунт заблокирован',
	'ID' => 'Логин',
	'Incorrect username or password' => 'Неверный логин или пароль',
	'Login' => 'Войти',
	'Password' => 'Пароль',
	'Remember me next time' => 'Запомнить меня',
	'Server' => 'Сервер',
	'Text from picture' => 'Текст на картинке',
];
