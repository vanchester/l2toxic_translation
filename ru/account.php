<?php
return [
	'<p>Here you can change password, change color of title and name, change name of the character, enchant items and skills, change attributes, edit augments and etc.</p><p>If you have any question, send email to administrator - <a href="mailto:blaze_l2toxic@hotmail.com">blaze_l2toxic@hotmail.com</a></p>' =>
			'<p>Здесь ты можешь изменить пароль, цвет ника или титула, имя персонажа, а также заточить вещи или скиллы, изменить атрибуты, аугментацию и многое другое.</p>
			<p>Если возникнут вопросы - пиши на email администратора - <a href="mailto:blaze_l2toxic@hotmail.com">blaze_l2toxic@hotmail.com</a></p>',
	'Welcome' => 'Привет',
	'Select the server' => 'Выберите сервер',
	'Select the character' => 'Выберите персонажа',
	'Password changed successfully' => 'Пароль успешно изменен',
	'There is an error. Try again later' => 'Произошла ошибка. Попробуйте повторить попытку позже',
	'Wrong current password' => 'Текущий пароль введен неверно',
];
