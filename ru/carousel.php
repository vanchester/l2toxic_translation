<?php
return [
	'Close' => 'Закрыть',
	'Next' => 'Далее',
	'Previous' => 'Назад',
	'Scroll to the left' => 'Прокрутить влево',
	'Scroll to the right' => 'Прокрутить вправо',
];
