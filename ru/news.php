<?php
return [
	'More news...' => 'Еще...',
	'News of the project' => 'Новости проекта',
	'Next' => 'Далее',
	'Previous' => 'Назад',
	'Scroll to the down' => 'Прокрутить вниз',
	'Scroll to the up' => 'Прокрутить вверх',
];
