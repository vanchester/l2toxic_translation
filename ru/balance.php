<?php
return [
	'<a href="/account/coins">Do you want to get more Golden Coins? Click here!</a>' => '<a href="/account/donation">Хотите получить больше Golden Coins? Кликайте сюда!</a>',
	'If you have any problems with donation, send private message to <a href="http://www.forum.prommorpg.com/private.php?do=newpm&u=1030">blaze</a> or to the email <a href="mailto:blaze_l2toxic@hotmail.com">blaze_l2toxic@hotmail.com</a>' =>
		'Если у вас созникли проблемы с пожертвованием, отправьте личное сообщение <a href="http://www.forum.prommorpg.com/private.php?do=newpm&u=1030">blaze</a> или на email <a href="mailto:blaze_l2toxic@hotmail.com">blaze_l2toxic@hotmail.com</a>',
	'Account balance' => 'Баланс аккаунта',
	'Do you want to give coins to selected char?' => 'Вы хотите выдать монеты выбранному персонажу?',
	'Fraud protection mode enabled! You can take coins in' => 'Включена защита от фрода. Вы можете получить коины через',
	'Selected character is online! Log out and try again or select another character' => 'Выбранный персонаж находится в игре. Разлогиньтесь или выберите другого персонаж',
	'Your balance is' => 'Ваш баланс',
];
